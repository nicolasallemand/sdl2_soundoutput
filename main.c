#include <stdio.h>
#include <stdbool.h>

#include <SDL.h>

#include "audio.h"

int main() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return 1;
    }
    SDL_Window* window = SDL_CreateWindow("sdl2_soundoutput",
        0, 0, 512, 512, 0);

    if (window == NULL) {
        SDL_Log("Unable to create window: %s", SDL_GetError());
        return 1;
    }

    // initialising audio:
    audio_init();

    // pushing 3 seconds of samples to the audio buffer:
    float x = 0;
    for (uint32_t i = 0; i < SAMPLE_RATE * 3; i++) {
        x += .05f;

        // audio_push expects a signed 16-bit value
        // note: "5000" here is just gain so that we will hear something
        const int16_t sample = sin(x) * 5000;

        // we have two "pushes" because we have one sample for left output,
        // and one sample for right output (stereo)
        audio_push(sample);
        audio_push(sample);
    }

    SDL_Event e;

    bool should_quit = false;
    while (!should_quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                should_quit = true;
            }
        }
    }

    audio_quit();
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
