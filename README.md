# SDL2 sound output

A simple example of sound output using SDL2 (with the new [SDL_QueueAudio](https://wiki.libsdl.org/SDL_QueueAudio) function, available in SDL 2.0.4+).
The program outputs a sine wave for three seconds.

To launch it (Linux + macOS), just run `make && ./sdl2_soundoutput`.
