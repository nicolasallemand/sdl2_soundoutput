#ifndef AUDIO_H
#define AUDIO_H

// Little audio wrapper around the SDL (2.0.4+) to easily
// output audio samples (in S16 format). It allows you to:
// - open a SDL audio device
// - send samples to the device
// - close the audio device

#include <stdbool.h>
#include <SDL.h>

#define SAMPLE_RATE 44100
#define NB_SAMPLES 1024
#define NB_CHANNELS 2

void audio_init();
void audio_quit();

void audio_pause(const bool pause);
void audio_clear();
void audio_push(const int16_t sample);

#endif // AUDIO_H
