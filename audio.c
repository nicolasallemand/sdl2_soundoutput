#include "audio.h"

static SDL_AudioDeviceID audio_device;

// opens a SDL audio device and starts playing
void audio_init() {
    SDL_AudioSpec audio_spec;
    SDL_zero(audio_spec);
    audio_spec.freq = SAMPLE_RATE;
    audio_spec.format = AUDIO_S16SYS;
    audio_spec.channels = NB_CHANNELS;
    audio_spec.samples = NB_SAMPLES;
    audio_spec.callback = NULL;

    audio_device = SDL_OpenAudioDevice(
        NULL, 0, &audio_spec, NULL, 0);

    if (audio_device == 0) {
        SDL_Log("failed to open audio: %s", SDL_GetError());
    }
    else {
        const char* driver_name = SDL_GetCurrentAudioDriver();
        SDL_Log("audio device has been opened (%s)", driver_name);
    }

    SDL_PauseAudioDevice(audio_device, 0); // start playing
}

// closes the audio device
void audio_quit() {
    SDL_CloseAudioDevice(audio_device);
    SDL_Log("audio device has been closed");
}

// pauses the audio device
void audio_pause(const bool pause) {
    SDL_PauseAudioDevice(audio_device, pause);
}

// clears all queued samples from the audio device
void audio_clear() {
    SDL_ClearQueuedAudio(audio_device);
}

// pushes one sample to the audio device
void audio_push(const int16_t sample) {
    SDL_QueueAudio(audio_device, &sample, sizeof(int16_t) * 1);
}
